Dans ce REPO il y'a 1 script permettant d'installer wordpress sur son ordinateur (init_instance_wp) et 1 script pour installer Mariadb (init_instance_mariadb)
Il y'a un Makefile permettant de crée 2 instances, les instances ont été configuré dans le fichier instances.tf et sont gérés par terraform

**_Installation Manuelle_**

Pour installer wordpress et mariadb manuellement il faut utiliser la commande:

```
./init_instance_wp
./init_instance_mariadb

```
**_Terraform Launcher_**

Sinon les instances qui sont générées par terraform installe mariadb et wordpress sur les instances sur un loadbalancer
Pour généré les deux instances web1 et web2 il faut executer 3 commandes:

```
make init
make plan
make apply
```

NOTE: Il est possible (et conseiller une fois que les instances ne sont plus utiles) de destruire les instances pour cela:

```
make destroy
```

**_Connection aux instances_**

Une fois les 2 instances crées, il faut se connecter en utilisant le script go :

```
./go web1 ou ./go web2
```
Pour se connecter sur la base de donnée mariadb il faut utiliser la commande :

```
./go mariadb
```

Pour se rendre sur les pages web des instances il faut utilise la commande : 

 ```
 curl https://lb_ip

 ```
