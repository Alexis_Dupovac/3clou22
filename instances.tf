resource "scaleway_instance_server" "web1" {
  name = "${local.team}-web1"

  project_id = var.project_id
  type       = "PRO2-XXS"
  image      = "debian_bullseye"

  tags = ["front", "web"]
 
  root_volume {
    size_in_gb = 10
  }

  user_data = {
    name        = "web1"
    myip        = "10.42.42.11"
    cloud-init = file("${path.module}/init_instance_wp")
    # "web1ip"   = "${scaleway_instance_server.web1.public_ip}"
  }

  security_group_id = scaleway_instance_security_group.sg-www.id

  private_network {
    pn_id = scaleway_vpc_private_network.myvpc.id
  }
}

resource "scaleway_instance_server" "web2" {
  name = "${local.team}-web2"

  project_id = var.project_id
  type       = "PRO2-XXS"
  image      = "debian_bullseye"

  tags = ["front", "web"]
  
  user_data = {
    name        = "web2"
    myip        = "10.42.42.12"
    cloud-init = file("${path.module}/init_instance_wp")
    }

  root_volume {
    size_in_gb = 10
  }
  private_network {
    pn_id = scaleway_vpc_private_network.myvpc.id
  }
  security_group_id = scaleway_instance_security_group.sg-www.id
}

resource "scaleway_instance_server" "MariaDB" {
  name = "${local.team}-MariaDB"

  project_id = var.project_id
  type       = "PRO2-XXS"
  image      = "debian_bullseye"

  tags = ["front", "web"] # see later

  root_volume {
    size_in_gb = 10
  }

  user_data = {
    name        = "MariaDB"
    myip        = "10.42.42.99"
    cloud-init = file("${path.module}/init_instance_mariadb")
  }
  security_group_id = scaleway_instance_security_group.sg-www.id
  private_network {
    pn_id = scaleway_vpc_private_network.myvpc.id
  }
}